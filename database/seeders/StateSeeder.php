<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // State::truncate();

        $csvFile = fopen(base_path("database/data/states.csv"), "r");

        $headerCsv = true;

        while (($data = fgetcsv($csvFile, 2000, ",")) !== FALSE) {

            if (!$headerCsv) {

                State::create([
                    'id' => $data['0'],
                    "code" => $data['1'],
                    "name" => $data['2'],
                    "created_at" => $data['3'],
                    "updated_at" => $data['4']
                ]);    

            }

            $headerCsv = false;
        }

        fclose($csvFile);
    }
}
