# how to

Proyecto creado en laravel 9 con PHP 8.0.

Pasos
- clonar el repositorio
- run composer install
- si no se creo un archivo .env, crearlo con `cp .env.example .env`
- crear una base de datos en mysql
-  modificar en el .env las variables `DB_HOST por tu host` `DB_DATABASE nombre de la base que creaste` `DB_USERNAME Y DB_PASSWORD por los datos de tu base de datos`
- correr desde una terminal con `php artisan migrate:fresh --seed` para popular la tabla de states
- correr desde una terminal con `php artisan serve` en el navegador correria en  `127.0.0.1:8000`
- Cuenta de 3 rutas: `/` y `/subscriptions` que son la misma y una `/subscriptions/lists` para ver el listado de las subscripciones.