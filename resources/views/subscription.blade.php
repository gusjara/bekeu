<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Subscription | Example</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Bootstrap 5 css CDN -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <!-- Select2 css CDN -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.3.0/dist/select2-bootstrap-5-theme.min.css" />
        
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

    </head>

    <body class="">
        
        <div class="col-md-6 mx-auto">
            
            <div class="card mt-4">
            
                <div class="card-header">
                    Complete your data
                </div>
            
                <div class="card-body">
                    <form action="{{ route('subscriptions.store')}}" class="row" method="POST">
                        @csrf
                        
                        @include('alerts')

                        <div class="row">                        
                            <div class="form-group row">
                                {{-- email data  --}}
                                <div class="col-md-6">
                                    <label for="email-address" class="form-label">Email address</label>
                                    <input type="email" name="email" class="form-control" id="email-address" placeholder="name@example.com" required>
                                </div>
                                {{-- state data --}}
                                <div class="col-md-4">
                                    <label for="select-contry" class="form-label">State</label>
                                    <select class="form-control" name="state_id" id="select-state" required>
                                        <option value="" selected disabled>Select State</option>
                                        @foreach ($states as $state)
                                        <option value="{{ $state->id }}">{{ $state->code }} - {{ $state->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-md-2 mt-4">
                                    <button type="submit" class="btn btn-success mt-1" id="save">Save</button>
                                </div>
                            </div>                        
                        </div>                        
                    </form>                
                </div>            
            </div>        
        </div>

        <!-- Bootstrap 5 js CDN -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

        <!-- jQuery CDN -->
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>

        <!-- select2 js CDN -->
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.full.min.js"></script>

        <script src="/scripts/home.js"></script>
        
    </body>

</html>
