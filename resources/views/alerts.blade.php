{{-- errors on form --}}
@if ($errors->any())
<div class="alert alert-danger" role="alert">
    <button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-label="Close"></button>
    <div>
        You have the following errors in your form
    </div>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
    
</div>
@endif

{{-- success --}}
@if (session('status'))
<div class="alert alert-success" role="alert">
    <button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-label="Close"></button>
    <h5>
        <i class="icon fas fa-check"></i>
        {{ session('status') }}
    </h5>
</div>
@endif

{{-- error on server --}}
@if (session('error'))
<div class="alert alert-danger" role="alert">
    <button type="button" class="btn-close float-end" data-bs-dismiss="alert" aria-label="Close"></button>
    <h5>
        <i class="icon fas fa-check"></i>
        {{ session('error') }}
    </h5>
</div>
@endif