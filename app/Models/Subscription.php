<?php

namespace App\Models;

use App\Models\Email;
use App\Models\State;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subscription extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email_id',
        'state_id',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * BelongsTo email
     *
     * @return BelongsTo
     */
    public function email()
    {
        return $this->BelongsTo(Email::class);
    }

    /**
     * BelongsTo state
     *
     * @return BelongsTo
     */
    public function state()
    {
        return $this->BelongsTo(State::class);
    }
}
