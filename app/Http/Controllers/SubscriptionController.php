<?php

namespace App\Http\Controllers;

use App\Models\Email;
use App\Models\State;
use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Http\Requests\SubscriptionRequest;

class SubscriptionController extends Controller
{
    /**
     * Form.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = State::all();
        return view('subscription', compact('states'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {   
        $subscriptions = Subscription::all();
        return view('subscription-list', compact('subscriptions'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\SubscriptionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubscriptionRequest $request)
    {
        $data = $request->except('_token');

        try {
            
            $email = Email::firstOrNew(['email' =>  request('email')]);
            $email->save();

            $subscription = Subscription::create([
                'email_id' => $email->id,
                'state_id' => $data['state_id']
            ]);

            return redirect()->route('subscriptions.index')->with('status', 'Your data was successfully saved.');
        } catch (\Throwable $th) {
            return redirect()->route('subscriptions.index')->with('error', 'We has an error try again soon.');
        }
        
    }

}
