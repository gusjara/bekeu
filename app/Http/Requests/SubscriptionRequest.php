<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        if ($this->method() == 'PATCH'){
            return [
                'email' => 'required|email',
                'state_id' => 'required',
            ];
        }
        return [
            'email' => 'required|email',
            'state_id' => 'required',           
        ];
    }

    // message
    public function messages(){
        return [
        'email.required' => 'The attribute :attribute is required.',
        'email.email' => 'The attribute :attribute must be an email.',
        'state_id.required' => 'The attribute :attribute is required.',

        ];
    }

    // rename attributes
    public function attributes(){
        return [
            'email' => 'E-mail',
            'state_id' => 'State',
        ];
    }
}
